class Order:
    def __init__(self, customer_email):
        self.customer_email = customer_email
        self.products = []
        self.purchased = False

    def add_product(self, product):
        self.products.append(product)

    def get_total_price(self):
        prices = []

        for n in self.products:
            prices.append(n.get_price())

        return sum(prices)

    def get_total_quantity_of_products(self):
        quantities = 0

        for n in self.products:
            quantities += n.quantity

        return quantities

    def purchase(self):
        self.purchased = True


class Product:
    def __init__(self, name, unit_price, quantity=1):
        self.name = name
        self.unit_price = unit_price
        self.quantity = quantity

    def get_price(self):
        return self.unit_price * self.quantity
