def square_given_number(number):
    return number ** 2


def volume_of_cuboid(number1, number2, number3):
    return number1 * number2 * number3


def degrees_Celsius_to_Fahrenheit(degrees):
    return degrees * 2 + 30


if __name__ == '__main__':
    zero_squared = square_given_number(0)
    sixteen_squared = square_given_number(16)
    float_squared = square_given_number(2.55)
    print("Square of 0 is", zero_squared)
    print("Square of 16 is", sixteen_squared)
    print("Square of float is", float_squared)

    volume_result = volume_of_cuboid(3,5,7)
    print(volume_result)
    print("Volume of a cuboid with sides:10,10,3", volume_of_cuboid(10,10,3))

    Fahrenheit_degrees = degrees_Celsius_to_Fahrenheit(20)
    print(Fahrenheit_degrees)